interface UserPayLoadInterface {
  username: string;
  password: string;
}

interface StaffMemberInterface {
  msg: string,
  code: number,
  isSuccess: boolean,
  list: [
    {
      id: number,
      memberState: string,
      memberType: string,
      name: string,
      username: string,
      isMan: string,
      dateBirth: string,
      phoneNumber: string,
      address: string,
    }
  ]
  totalCount: number,
  totalPage: number,
  currentPage: number,
}

interface noticeInterface {
  title: string,
  content: string,
  multipartFile : string
}

interface askAndAnswerInterface {
  id: number,
  memberId: number,
  memberName: string,
  isAnswer: string,
  title: string,
  content: string,
  questionImgUrl: string,
  dateMemberAsk: string,
  comment: {
    adminId: number,
    adminName: string,
    answerTitle: string,
    answerContent: string,
    answerImgUrl: string,
    dateAnswer: string
  }
}
