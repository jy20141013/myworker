import {defineStore, storeToRefs} from "pinia";
import {useBusStore} from "~/store/business";
interface requestInterface{
  refuseReason: string
}

// auth 스토어
export const useRequestStore = defineStore('request', {
  state: () => ({
  }),
  actions: {
    async agreeRequest(requestId: string) {
      const token = useCookie('token');
      await useFetch(`http://versuss.store:8080/v1/request/Agree/request-id/${requestId}`, {
        method: 'PUT',
        headers: {
          'Authorization': `Bearer ${token.value}`,
        },
      });
    },

    async refuseRequest(requestId: string, {refuseReason} : requestInterface) {
      const token = useCookie('token');
      await useFetch(`http://versuss.store:8080/v1/request/RefuseReason/request-id/${requestId}`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
        body: {
          refuseReason
        }
      });
    },
  }
})
