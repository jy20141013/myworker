import { defineStore } from "pinia";

interface askResponse {
  memberName: string,
  isAnswer: string,
  title: string,
  content: string
  dateMemberAsk: string
}

// auth 스토어
export const useAskStore = defineStore('memberAsk', {
  state: () => ({
      askList: [] as askResponse[],
      askDetail: [],
      comment: []
  }),
  actions: {
    async getAsk(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/member-ask/all/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.askList = data.value.list;
      }
    },

    async getDetail(memberAskId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/member-ask/detail/member-ask-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.askDetail = data.value.data;
        this.comment = data.value.data.comment;
      }
    }
  }
})
