import { defineStore } from "pinia";

export const useAuthStore = defineStore('auth', {
  state: () => ({
    authenticated: false,
    loading: false,
  }),
  actions: {
    async authenticateUser({ username, password }: UserPayLoadInterface) {
      const { data, pending } : any = await useFetch('http://versuss.store:8080/v1/login/web/admin', {
        method: 'post',
        headers: { 'content-type': 'application/json' },
        body: {
          username,
          password,
        },
      });
      this.loading = pending;
      const token = useCookie('token');

      if (data.value) {
        token.value = data?.value?.data?.token;
        this.authenticated = true;
      }
    },
    logUserOut() {
      const token = useCookie('token');
      this.authenticated = false;
      token.value = null;
    },
  },
});
