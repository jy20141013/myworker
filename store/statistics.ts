import { defineStore } from "pinia";

// auth 스토어
export const useStaticStore = defineStore('static', {
  state: () => ({
    dayStatics: {
      joinDatasets: [],
      businessDatasets: [],
      newWriteDatasets: [],
      newInquiryDatasets: [],
    },
    monthStatics: {
      joinDatasets: [],
      businessDatasets: [],
      newWriteDatasets: [],
      newInquiryDatasets: [],
    },
  }),
  actions: {
    async getDayStatic() {
      const token = useCookie('token');
      const { data } : any = await useFetch('http://versuss.store:8080/v1/chart/day-join-chart', {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.dayStatics = data.value.data;
        this.dayStatics.joinDatasets = data.value.data.joinDatasets;
      }
    },

    async getMonthStatic() {
      const token = useCookie('token');
      const { data } : any = await useFetch('http://versuss.store:8080/v1/chart/month-join-chart', {
        method: 'get',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data.value) {
        this.monthStatics = data.value.data;
      }
    }
  },
})
