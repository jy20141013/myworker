import { defineStore } from "pinia";

interface memberResponse {
  id: number
  memberState: string,
  memberType: string,
  name: string,
  username: string,
  isMan: string,
  dateBirth: string,
  phoneNumber: string,
  address: string,
  dateMember: string
}

// auth 스토어
export const useStaffStore = defineStore('staffMember', {
  state: () => ({
    memberList: [] as memberResponse[],
    memberDetail: [] as memberResponse[],
    businessMemberDetails: []
  }),
  actions: {
    async getMember(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/member/all/general/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.memberList = data.value.list;
      }
    },
    async getDetail(memberId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/member/get/general-business/member-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.memberDetail = data.value.data;
        this.businessMemberDetails = data.value.data.businessMemberDetails;
      }
    }
  }
})
