import { defineStore } from "pinia";

interface memberResponse {
  businessName: string,
  businessNumber: string,
  ownerName: string,
  businessImgUrl: string,
  businessType: string,
  businessLocation: string,
  businessEmail: string,
  businessPhoneNumber: string,
  isActivity: string,
  dateJoinBusiness: string,
  isApprovalBusiness: string,
  dateApprovalBusiness: string,
  refuseReason: string,
  refuseFix: string,
  reallyLocation: string,
}

// auth 스토어
export const useBusStore = defineStore('business', {
  state: () => ({
    businessList: [] as memberResponse[],
    businessDetail: [] as memberResponse[],
    response: '',
    businessMember: [],
    staffDetail: []
  }),
  actions: {
    async busList(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/business/all/${pageNum}`, {
        method: 'get',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.businessList = data.value.list;
      }
    },

    async busDetail(businessId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/business/detail/business-id/${id}`, {
        method: 'get',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.businessDetail = data.value.data;
        this.response = data.value.data.requestItem.id;
      }
    },

    async getStaff(businessId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/business/detail/business/member/business-id/${id}`, {
        method: 'get',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.staffDetail = data.value.data.memberList;
      }
    },

    async busMember(businessId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/business/detail/business-id/${id}`, {
        method: 'get',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.businessDetail = data.value.data;
        this.response = data.value.data.requestItem.id;
      }
    }
  }
})
