import { defineStore } from "pinia";

interface memberResponse {
  memberState: string
  name: string
  username: string
  isMan: boolean
  dateBirth: string
  phoneNumber: string
  dateMember: string
}

// auth 스토어
export const useOwnerStore = defineStore('ownerMember', {
  state: () => ({
   memberList: [] as memberResponse[],
    memberDetail: [],
    businessDetailResponse: []

  }),
  actions: {
    async getMembers(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/member/all/owner/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.memberList = data.value.list;
      }
    },

    async getMember(memberId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/member/get/boss-business/member-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data.value) {
        this.memberDetail = data.value.data;
        this.businessDetailResponse = data.value.data.businessDetailResponse;
      }
    }
  },
})
