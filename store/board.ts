import { defineStore } from "pinia";
import BoardList from "~/pages/board/boardList.vue";

interface BoardContent {
  id: number,
  category: string,
  title: string,
  content: string,
  boardImagUrl: string,
  dateBoard: string,
  memberId: number
}

// auth 스토어
export const useBoardStore = defineStore('board', {
  state: () => ({
    boardList: [] as BoardContent[],
    boardDetail: [],
    comment: []
  }),
  actions: {
    async getBoard(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/board/all/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.boardList = data.value.list;
      }
    },

    async getDetail(boardId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/board/detail/board-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.boardDetail = data.value.data;
        this.comment = data.value.data.comments;
      }
    },

    async delBoard(boardId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/board/del/board-id/${id}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
    },

    async delComment(boardId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/board/del/board-id/${id}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
    }
  }
})
