import { defineStore } from 'pinia'

interface IUser {
  answerTitle: string
  answerContent: string,
  multipartFile: string
}

// auth 스토어
export const useAskAnswerStore = defineStore('askAnswer', {
  state: () => {
    return {
    }
  },
  actions: {
    async setAnswer(memberAskId : string, { answerTitle, answerContent, multipartFile }: IUser){
      const token = useCookie('token');
      const { id } = useRoute().params
      const formData = new FormData();
      formData.append('answerTitle', answerTitle);
      formData.append('answerContent', answerContent);

      if (multipartFile) {
        formData.append('multipartFile', multipartFile);
      }

      await useFetch(`http://versuss.store:8080/v1/ask-answer/member-ask/${id}`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token.value}`,
        },
        body: formData
      });
    },

    async putAnswer(memberAskId : string, { answerTitle, answerContent, multipartFile }: IUser){
      const token = useCookie('token');
      const { id } = useRoute().params
      await useFetch(`http://versuss.store:8080/v1/ask-answer/member-ask-id/${id}`, {
        method: 'PUT',
        headers: {
          'Authorization': `Bearer ${token.value}`,
        },
        body: {
          answerTitle,
          answerContent,
          multipartFile
        },
      });
    }
  }
})
