import { defineStore } from "pinia";


// auth 스토어
export const useReportStore = defineStore('report', {
  state: () => ({
    reportBoardList: [],
    reportCommentList: [],
    reportBoardDetail: [],
    reportCommentDetail: []
  }),
  actions: {
    async getReportBoard(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/report-board/all/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.reportBoardList = data.value.list;
      }
    },
    async getReportComment(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/report-comment/all/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.reportCommentList = data.value.list;
      }
    },
    async getRepoBoardDetail(reportboardId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/report-board/detail/report-boradId/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.reportBoardDetail = data.value.data;
      }
    },
    async getRepoCommentDetail(boardId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/report-comment/detail/report-comment-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
      if (data) {
        this.reportCommentDetail = data.value.data;
      }
    }
  }
})
