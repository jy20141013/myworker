import { defineStore } from "pinia";
import {$fetch} from "ofetch";

interface noticeResponse {
  id: number
  memberType: string,
  title: string,
  dateNotice: string,
  content: string,
  noticeImgUrl: string
}

// auth 스토어
export const useNoticeStore = defineStore('notice', {
  state: () => ({
    noticeList: [] as noticeResponse [],
    noticeDetail: [],
  }),
  actions: {
    async getNotice(pageNum : number) {
      const token = useCookie('token');
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/notice/all/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
      if (data.value) {
        this.noticeList = data.value.list;
      }
    },

    async postNotice({title, content, multipartFile} : noticeInterface) {
      const token = useCookie('token');
      const formData = new FormData();
      formData.append('title', title);
      formData.append('content', content);

      if (multipartFile) {
        formData.append('multipartFile', multipartFile);
      }

      await useFetch('http://versuss.store:8080/v1/notice/new', {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token.value}`,
        },
        body: formData
      });
    },

    async getDetail(noticeId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const { data } : any = await useFetch(`http://versuss.store:8080/v1/notice/detail/notice-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`,
        },
      });
      if (data) {
        this.noticeDetail = data.value.data;
      }
    },

    async putNotice(noticeId : string, {title, content, multipartFile} : noticeInterface) {
      const token = useCookie('token');
      const { id } = useRoute().params
      await useFetch(`http://versuss.store:8080/v1/notice/change/notice-id/${id}`, {
        method: 'PUT',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
        body: {
          title,
          content,
          multipartFile
        }
      });
    },
    async deleteNotice(noticeId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      await useFetch(`http://versuss.store:8080/v1/notice/notice-id/${id}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });
    },
  }
})
